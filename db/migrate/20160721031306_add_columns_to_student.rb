class AddColumnsToStudent < ActiveRecord::Migration
  def change
    add_column :students, :emergency_contact_name, :string
    add_column :students, :emergency_contact_relationship, :string
    add_column :students, :emergency_contact_phone, :string
    add_column :students, :phone, :string
    add_column :students, :address, :string
    add_column :students, :age, :integer
    add_column :students, :birthday, :string
  end
end
