class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :classification
      t.string :gender
      t.string :zip
      t.string :city
      t.string :state

      t.timestamps null: false
    end
  end
end
