var gulp = require('gulp');
var webserver = require('gulp-webserver');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-ruby-sass');

var template = require('gulp-template-compile');
var concat = require('gulp-concat');

// Build Dependencies
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');

// Style Dependencies
var less = require('gulp-less');
var prefix = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');

// Development Dependencies
var jshint = require('gulp-jshint');

// Test Dependencies
var mochaPhantomjs = require('gulp-mocha-phantomjs');

var htmltojson = require('gulp-html-to-json');

gulp.task('markdown', function () {
	gulp.src('app/assets/javascripts/backbone/templates/**/*.html')
		.pipe(template())
		.pipe(concat('templates.js'))
		.pipe(gulp.dest('app/assets/javascripts/backbone/templates'));
});

gulp.task('webserver', function() {
  return gulp.src('app/assets/javascripts/backbone')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: false
    }));
});

gulp.task('styles', function() {
  return sass('./app/assets/stylesheets/sass/**/*.scss', { style: 'expanded' })
    .pipe(gulp.dest('app/assets/stylesheets/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('app/assets/stylesheets/css'));
});

gulp.task('browserify-client', function() {
  return gulp.src('./app/assets/javascripts/backbone/**/*.js')
    .pipe(browserify({
      insertGlobals: true
    }))
    .pipe(gulp.dest('build'))
    .pipe(gulp.dest('public/javascripts'));
});

gulp.task('watch', function() {
  gulp.watch('app/assets/javascripts/backbone/**/**/*.html', ['markdown']);
	gulp.watch('app/assets/stylesheets/sass/**/*.scss', ['styles']);
});

gulp.task('build', ['markdown', 'styles']);
gulp.task('default', ['build', 'webserver', 'watch']);
