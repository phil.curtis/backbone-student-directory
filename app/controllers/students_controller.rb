class StudentsController < ApplicationController
  respond_to :json
  def index
    respond_with Student.all
  end

  def show
    respond_with Student.find(params[:id])
  end

  def new
    respond_with Student.new
  end

  def create
    @student = Student.new(student_params)
    # if @student.save
    #   respond_with json: @student, status: 201
    # end
  end

  def update
    @student = Student.find(params[:id])
    # if @student.update(student_params)
    #   respond_with json: @student, status: 201
    # end
  end

  def destroy
    @student = Student.find(params[:id])
    # if @student.destroy
    #   respond_with json: @student, status: 201
    # end
  end


private
  def student_params
    params.require(:student).permit(:first_name, :last_name, :email, :classification, :gender, :zip, :city, :state)
  end
end
