class Student < ActiveRecord::Base
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :classification, presence: true
  validates :email, presence: true
  validates :gender, presence: true
  validates :zip, presence: true
  validates :city, presence: true
  validates :state, presence: true

  
end
