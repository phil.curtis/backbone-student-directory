var InfoCard = BaseView.extend({
  template: window['JST']['home/info_card.html'],

  initialize: function(options) {
    this.el = options.el;
  },

  render: function() {
    this.$el.html(this.template);
    return this;
  }
})
