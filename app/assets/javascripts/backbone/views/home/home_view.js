var HomeView = BaseView.extend({
  template: window['JST']['home/home.html'],

  initialize: function() {

  },

  render:  function() {
    this.$el.html(this.template);
    return this;
  }
})
