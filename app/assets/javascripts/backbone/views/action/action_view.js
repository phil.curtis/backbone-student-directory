var ActionView = BaseView.extend({
  el: '.action-button-mount',

  template: window['JST']['action/action_button.html'],

  events: {
  'click .action-button': 'action'
  },

  initialize: function(opts) {
    this.canRender = opts.canRender;
    this.type = opts.type;
    this.action = opts.action;
  },

  render: function() {
    if (this.canRender) {
      switch (this.type) {
        case "edit":
          this.$el.html(this.template({class: "pencil"}));
          break;
        case "add":
          this.$el.html(this.template({class: "plus"}))
          break;
        default:
          console.log("nope");
      }
    }
  },

  action: function() {
    this.action();
  }
})
