var NavigationView = BaseView.extend({
  template: window["JST"]["navigation/navigation.html"],

  el: 'body',

  initialize: function() {

  },

  loadView: function() {

  },

  render: function() {
    this.$el.html(this.template);
    return this;
  }
})
