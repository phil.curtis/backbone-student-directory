var StudentShowView = BaseView.extend({
  template: window['JST']['students/show.html'],

  className: 'student-show',

  initialize: function() {

  },

  render: function() {
    this.$el.html('');
    this.$el.html(this.template({'data': this.model}));
    var editButton = new ActionView({canRender: "true", type: "edit"});
    editButton.render();
    return this;
  }
})
