var StudentIndexView = BaseView.extend({
  template: window["JST"]["students/index.html"],

  initialize: function(opts) {

  },

  render: function() {
    this.$el.html('');
    this.$el.html(this.template)
    if (this.collection !== null && this.collection !== undefined) {
      this.collection.each((student) => {
        var row = new StudentIndexItemView({model: student, el: '.students-container'});
        row.render();
      })
    }
    var addButton = new ActionView({canRender: "true", type: "add", action: this.addStudent});
    addButton.render();
    return this;
  },

  addStudent: function() {

  }

})
