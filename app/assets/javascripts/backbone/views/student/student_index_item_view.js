var StudentIndexItemView = BaseView.extend({
  template: window['JST']['students/index_item.html'],

  initialize: function(opts) {

  },

  render: function() {
    this.$el.append(this.template({'data': this.model}));
    this.$el.find(`[name='student[${this.model.get("id")}]'] .view-profile`).attr('href', '#/students/' + this.model.get('id'));
    return this;
  }
})
