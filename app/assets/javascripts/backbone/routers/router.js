function AppRouter () {
  var AppRouter = Backbone.Router.extend({
    routes: {
      '': "default",
      'students': 'studentIndex',
      'students/:id': 'studentShow',
      'students/new': 'studentNew'
    },

    initialize: function() {
      this.view = new NavigationView();
      this.view.render();
    },

    default: function() {
      this.homeView = new HomeView();
      this.homeView.render();

    },

    studentShow: function(_id) {
      new Student({id: _id}).fetch({
        success: function(result) {
          this.view = new StudentShowView({model: result});
          this.view.render();
        },
        error: function() {
          console.log("error");
        }
      })
    },

    studentIndex: function() {
      new StudentCollection().fetch({
        success: function(result) {
          this.view = new StudentIndexView({collection: result, router: this});
          this.view.render();
        }
      })
    },

    studentNew: function() {
      this.studentShow()
    }

  })

  var router = new AppRouter();
  router.on("route:students", function () {
      this.studentIndex();
  });

  router.on("route:students/:id", function(_id) {
    this.studentShow(_id);
  })

  router.on("route:students/new", function() {
    this.studentNew();
  })

  return router;
}
