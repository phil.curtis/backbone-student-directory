(function() {
window["JST"] = window["JST"] || {};

window["JST"]["navigation/navigation.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="navigation">\n  <div class="container-fluid dark-row">\n    <div class="row">\n      <div class="col-md-12 dark-primary-row">\n      </div>\n    </div>\n  </div>\n  <nav class="navbar navbar-default directory-nav navbar-fixed-top">\n    <div class="container-fluid">\n      <div class="navbar-header">\n        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">\n          <span class="sr-only">Toggle navigation</span>\n          <span class="icon-bar"></span>\n          <span class="icon-bar"></span>\n          <span class="icon-bar"></span>\n        </button>\n        <a class="navbar-brand" href="#">\n          Directory\n        </a>\n      </div>\n      <div class="collapse navbar-collapse">\n        <ul class="nav navbar-nav">\n          <li><a href="#/students">Students</a></li>\n          <li><a href="/faculty">Faculty</a></li>\n          <li><a href="/staff">Staff</a></li>\n          <li><a href="/alumni">Alumni</a></li>\n        </ul>\n      </div>\n    </div>\n  </nav>\n  <div id="nav-spacer" class=""></div>\n</div>\n<div class="container-fluid view-container">\n\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["action/action_button.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="action-button-container">\n  <button class="action-button" type="button" name="button"><i class="fa fa-lg fa-' +
((__t = ( obj.class )) == null ? '' : __t) +
'"></i></button>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["students/index.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="student-index">\n  <h1>Students</h1>\n  <div id="student-results" class="row">\n    <div class="col-md-8">\n      <div class="students-container">\n      </div>\n    </div>\n    <div class="col-md-4">\n      <div class="row">\n        <div class="col-md-12">\n          <div class="filter">\n\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="action-button-mount">\n\n  </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["students/index_item.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="student-index-item">\n  <div class="student-card" name="student[' +
((__t = (obj.data.get('id'))) == null ? '' : __t) +
']">\n    <div class="row">\n      <div class="col-md-10">\n        <div class="row">\n          <div class="col-md-12">\n            <div class="row">\n              <div class="col-md-6 student-name">\n                <span>' +
((__t = ( obj.data.get("first_name") + " " +  obj.data.get("last_name") )) == null ? '' : __t) +
'</span>\n              </div>\n              <div class="col-md-4 pull-right text-right profile-link">\n                <a href="" class="view-profile"><i class="fa fa-lg fa-user"></i>View Profile</a>\n              </div>\n            </div>\n            <div class="row student-info">\n              <div class="col-md-3 pull-left">\n                <strong>Class: </strong>\n                <span>' +
((__t = ( obj.data.get("classification") )) == null ? '' : __t) +
'</span>\n              </div>\n              <div class="col-md-3 pull-left">\n                <strong>Gender: </strong>\n                <span>' +
((__t = ( obj.data.get("gender") )) == null ? '' : __t) +
'</span>\n              </div>\n              <div class="col-md-6 student-email pull-left">\n                <strong>Email: </strong>\n                <span>' +
((__t = ( obj.data.get("email") )) == null ? '' : __t) +
'</span>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class="col-md-2">\n        <div class="student-icon pull-right"></div>\n      </div>\n    </div>\n  </div>\n\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["students/show.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="row student-show">\n  <div class="student-show-info col-md-10 col-md-offset-1 text-center">\n    <div class="row">\n      <div class="col-md-12 show-info-container">\n        <h1>' +
((__t = ( obj.data.get("first_name") + " " + obj.data.get("last_name") )) == null ? '' : __t) +
'</h1>\n      </div>\n    </div>\n    <div class="row text-center">\n      <div class="col-md-4 col-md-offset-2 text-left">\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].id">Student ID:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].id">' +
((__t = (obj.data.get("id"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].gender">Gender:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].gender">' +
((__t = (obj.data.get("gender"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].classification">Classification:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].classification">' +
((__t = (obj.data.get("classification"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].age">Age:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].age">' +
((__t = (obj.data.get("age"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].birthday">Birthday:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].birthday">' +
((__t = (obj.data.get("birthday"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].email">Email:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].email">' +
((__t = (obj.data.get("email"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].phone">Phone:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].phone">' +
((__t = (obj.data.get("phone"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n      </div>\n      <div class="col-md-4 col-md-offset-2 text-left">\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].emergency_contact_name">Emergency Contact Name:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].emergency_contact_name">' +
((__t = (obj.data.get("emergency_contact_name"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].emergency_contact_relationship">Emergency Contact Relationship:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].emergency_contact_relationship">' +
((__t = (obj.data.get("emergency_contact_relationship"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].emergency_contact_phone">Emergency Contact Phone:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].emergency_contact_phone">' +
((__t = (obj.data.get("emergency_contact_phone"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].Address">Address:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].Address">' +
((__t = (obj.data.get("Address"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].city">City:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].city">' +
((__t = (obj.data.get("city"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].state">State:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].state">' +
((__t = (obj.data.get("state"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12 show-info-container">\n            <label class="student-info-label" for="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].zip">Zip:</label>\n            <span class="student-info-piece" name="student[' +
((__t = ( obj.data.get("id") )) == null ? '' : __t) +
'].zip">' +
((__t = (obj.data.get("zip"))) == null ? '' : __t) +
'</span>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="action-button-mount">\n\n  </div>\n</div>\n';

}
return __p
}})();
(function() {
window["JST"] = window["JST"] || {};

window["JST"]["home/home.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="home">\n  <div class="row heading-spacer">\n    <div class="col-md-4 col-md-offset-1 home-news text-center">\n      <div class="row news-title">\n        <div class="col-md-12">\n          <div class=" text-center">\n            <h1>News</h1>\n          </div>\n        </div>\n      </div>\n      <div class="row">\n        <div class="col-md-12 news-content-container">\n          content\n        </div>\n      </div>\n    </div>\n    <div class="col-md-4 col-md-offset-2 home-events text-center">\n      <div class="row events-title">\n        <div class="col-md-12">\n          <div class=" text-center">\n            <h1>Events</h1>\n          </div>\n        </div>\n      </div>\n      <div class="row">\n        <div class="col-md-12 events-content-container">\n          content\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>\n';

}
return __p
}})();